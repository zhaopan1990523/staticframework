package org.zp.study.core.base;

import com.baomidou.mybatisplus.plugins.Page;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author 赵攀
 *业务层结果实例
 */
public class Result  implements Serializable {
    private Long id;
    private Bean model;
    private Map<?, ?> map;
    private Page<?> page;
    private List<?> list;
    private Object object;

    public Long getId() {
        return id;
    }

    public Result setId(Long id) {
        this.id = id;
        return this;
    }

    public Bean getModel() {
        return model;
    }

    public Result setModel(Bean model) {
        this.model = model;
        return this;
    }

    public Map<?, ?> getMap() {
        return map;
    }

    public Result setMap(Map<?, ?> map) {
        this.map = map;
        return this;
    }

    public Page<?> getPage() {
        return page;
    }

    public Result setPage(Page<?> page) {
        this.page = page;
        return this;
    }

    public List<?> getList() {
        return list;
    }

    public Result setList(List<?> list) {
        this.list = list;
        return this;
    }

    public Object getObject() {
        return object;
    }

    public Result setObject(Object object) {
        this.object = object;
        return this;
    }

    public Result(Object result) {
        if (result instanceof Long) {
            this.id = (Long) result;
        } else if (result instanceof Bean) {
            this.model = (Bean) result;
        } else if (result instanceof Page) {
            this.page = (Page<?>) result;
        } else if (result instanceof Map<?, ?>) {
            this.map = (Map<?, ?>) result;
        } else if (result instanceof List<?>) {
            this.list = (List<?>) result;
        } else {
            this.object = result;
        }
    }
}
