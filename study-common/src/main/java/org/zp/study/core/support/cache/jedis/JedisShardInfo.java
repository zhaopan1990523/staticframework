package org.zp.study.core.support.cache.jedis;

import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaopan
 * @version 2017年3月28日
 * 扩展JedisShardInfo类
 */
public class JedisShardInfo extends redis.clients.jedis.JedisShardInfo {

    public JedisShardInfo(String host, int port) {
        super(host, port);
    }
    
    public void setPassword(String password) {
        if (StringUtils.isNotBlank(password)) {
            super.setPassword(password);
        }
    }
}
