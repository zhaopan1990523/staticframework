package org.zp.study.core.support.cache.jedis;


import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.Random;
//redis锁
public class RedisLock {

    /** 加锁标志 */
    public static final String LOCKED = "TRUE";
    /** 毫秒与毫微秒的换算单位 1毫秒 = 1000000毫微秒 */
    public static final long MILLI_NANO_CONVERSION = 1000 * 1000L;
    /** 默认超时时间（毫秒） */
    public static final long DEFAULT_TIME_OUT = 1000;
    public static final Random RANDOM = new Random();
    /** 锁的超时时间（秒），过期删除 */
    public static final int EXPIRE = 3 * 60;

    private static ShardedJedisPool shardedJedisPool;
    private ShardedJedis jedis;
    private String key;
    // 锁状态标志
    private boolean locked = false;
    /**
     * This creates a RedisLock
     * @param key key
     * @param jedis
     */

    public RedisLock(String key,  ShardedJedis jedis) {
        this.key = key + "_lock";
        this.jedis = jedis;
        //this.jedis =shardedJedisPool.getResource();
    }
    public RedisLock(ShardedJedisPool shardedJedisPool){
        this.jedis =shardedJedisPool.getResource();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    /**
     * 加锁
     * 应该以：
     * lock();
     * try {
     *      doSomething();
     * } finally {
     *      unlock()；
     * }
     * 的方式调用
     * @param timeout 超时时间
     * @return 成功或失败标志
     */
    public  boolean lock(long timeout) {
        long nano = System.nanoTime();
        timeout *= MILLI_NANO_CONVERSION;
        try {
            while ((System.nanoTime() - nano) < timeout) {
                if (this.jedis.setnx(this.key, LOCKED) == 1) {
                    this.jedis.expire(this.key, EXPIRE);
                    this.locked = true;
                    return this.locked;
                }
                // 短暂休眠，避免出现活锁
                Thread.sleep(3, RANDOM.nextInt(500));
            }
        } catch (Exception e) {
            throw new RuntimeException("Locking error", e);
        }
        return false;
    }

    /**
     * 加锁
     * 应该以：
     * lock();
     * try {
     *      doSomething();
     * } finally {
     *      unlock()；
     * }
     * 的方式调用
     * @param timeout 超时时间
     * @param expire 锁的超时时间（秒），过期删除
     * @return 成功或失败标志
     */
    public boolean lock(long timeout, int expire) {
        long nano = System.nanoTime();
        timeout *= MILLI_NANO_CONVERSION;
        try {
            while ((System.nanoTime() - nano) < timeout) {
                if (this.jedis.setnx(this.key, LOCKED) == 1) {
                    this.jedis.expire(this.key, expire);
                    this.locked = true;
                    return this.locked;
                }
                // 短暂休眠，避免出现活锁
                Thread.sleep(3, RANDOM.nextInt(500));
            }
        } catch (Exception e) {
            throw new RuntimeException("Locking error", e);
        }
        return false;
    }

    /**
     * 加锁
     * 应该以：
     * lock();
     * try {
     *      doSomething();
     * } finally {
     *      unlock()；
     * }
     * 的方式调用
     * @return 成功或失败标志
     */
    public boolean lock() {
        return lock(DEFAULT_TIME_OUT);
    }

    /**
     * 解锁
     * 无论是否加锁成功，都需要调用unlock
     * 应该以：
     * lock();
     * try {
     *      doSomething();
     * } finally {
     *      unlock()；
     * }
     * 的方式调用
     */
    public void unlock() {
        try {
            if (this.locked) {
                this.jedis.del(this.key);
            }
        } finally {
            this.jedis.close();
        }
    }

    /** 获取锁
    public  boolean getLock(String key) {
        if (!jedis.exists(key)) {
            synchronized (CacheUtil.class) {
                if (!jedis.exists(key)) {
                    if (jedis.setnx(key, String.valueOf(System.currentTimeMillis()))==1) {
                        return true;
                    }
                }
            }
        }
        int expires = 1000 * 60 * 3;
        String currentValue = (String) jedis.get(key);
        if (currentValue != null && Long.parseLong(currentValue) < System.currentTimeMillis() - expires) {
            unlock();
            return getLock(key);
        }
        return false;
    }
     */
}