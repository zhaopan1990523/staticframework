package org.zp.study.core.support.cache.jedis;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisClusterConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.stereotype.Component;
import org.zp.study.core.util.InstanceUtil;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.List;


/**
 * @author 赵攀
 *         操作redis的模版，重写自带的RedisTemplate
 */
@Component
public class JedisTemplate {
    //public class JedisTemplate extends RedisTemplate {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private JedisConnectionFactory jedisConnectionFactory;

    private ShardedJedisPool shardedJedisPool;

    private Integer defaultExpiration = 600;
    public JedisTemplate(){}
    public JedisTemplate(JedisConnectionFactory jedisConnectionFactory){
        this.jedisConnectionFactory=jedisConnectionFactory;
    }
    // 获取Jedis对象
    public ShardedJedis getJedis() {
        if (shardedJedisPool == null) {
            synchronized (defaultExpiration) {
                if (shardedJedisPool == null) {
                    JedisPoolConfig poolConfig = jedisConnectionFactory.getPoolConfig();
                    RedisClusterConnection connection=jedisConnectionFactory.getClusterConnection();
                    redis.clients.jedis.JedisShardInfo shardInfo = jedisConnectionFactory.getShardInfo();
                    List<JedisShardInfo> list = InstanceUtil.newArrayList(shardInfo);
                    shardedJedisPool = new ShardedJedisPool(poolConfig, list);
                }
            }
        }
        return shardedJedisPool.getResource();
    }


    public <K> K run(Object key, Executor<K> executor, Integer... expire) {

        return run(key, executor, true, expire);
    }

    //    public <K> K run(byte[] key, Executor<K> executor, Integer... expire) {
//        return run(key, executor, true, expire);
//        return run(key, executor, true, expire);
//    }
//
//    public <K> K run(byte[] key, Executor<K> executor,boolean expireTime, Integer... expire) {
//        return run(key, executor, expireTime, expire);
//    }
    public <K> K run(Object key, Executor<K> executor, boolean expireTime, Integer... expire) {
        ShardedJedis jedis = getJedis();
        if (jedis == null) {
            return null;
        }
        try {
            K result = executor.execute(jedis);
            if (expireTime && (key instanceof String || key instanceof byte[])) {
                if (jedis.exists(key.toString())) {
                    if (expire == null || expire.length == 0) {
                        jedis.expire(key.toString(), defaultExpiration);
                    } else if (expire.length == 1) {
                        jedis.expire(key.toString(), expire[0]);
                    }
                }
            }
            return result;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }


}

