package org.zp.study.core.support.cache.jedis;

import redis.clients.jedis.ShardedJedis;

/**
 * @author zhaopan
 * @version 2016年5月20日 下午3:19:19
 * redis执行器
 */
public interface Executor<K> {
	K execute(ShardedJedis jedis);
}
