package org.zp.study.core.provider;

import org.zp.study.core.base.Parameter;
import org.zp.study.core.base.Result;

/**
 * @author zhaopan
 * @version 2016年5月20日 下午3:19:19
 * 提供器,可以作为对外接口，让控制层与业务层解耦和
 */
public interface BaseProvider {
    Result execute(Parameter parameter);
}
