package org.zp.study.core.support.token;

import com.alibaba.fastjson.JSON;
import org.zp.study.core.Constants;
import org.zp.study.core.support.cache.jedis.CacheFactory;


public class TokenUtil {
	public static void setTokenInfo(String token, String value) {
		try {
			Token tokenInfo = new Token();
			tokenInfo.setTime(System.currentTimeMillis());
			tokenInfo.setValue(value);
			CacheFactory.getJedisCacheManager().hset(Constants.TOKEN_KEY, token, JSON.toJSONString(tokenInfo));
		} catch (Exception e) {
			throw new RuntimeException("保存token失败，错误信息：", e);
		}
	}

	public static void delToken(String token) {
		try {
			CacheFactory.getJedisCacheManager().hdel(Constants.TOKEN_KEY, token);
		} catch (Exception e) {
			throw new RuntimeException("删除token失败，错误信息：", e);
		}
	}

	public static Token getTokenInfo(String token) {
		String value = (String) CacheFactory.getJedisCacheManager().hget(Constants.TOKEN_KEY, token);
		Token tokenInfo = JSON.parseObject(value, Token.class);
		return tokenInfo;
	}
}
