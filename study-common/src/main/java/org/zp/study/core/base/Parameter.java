package org.zp.study.core.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author zhaopan
 * @since 2017年2月14日 下午3:27:17
 * 业务层整合参数实例
 */
@SuppressWarnings("serial")
public class Parameter implements Serializable {
    private String service;
    private String method;

    private Long id;
    private Bean model;
    private Map<?, ?> map;
    private List<?> list;
    private Object object;
    public Parameter() {
    }

    public Parameter(String service, String method) {
        this.service = service;
        this.method = method;
    }

    public Object getObject() {
        return object;
    }

    public Parameter setObject(Object object) {
        this.object = object;
        return this;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMethod() {
        return method;
    }

    public Parameter setMethod(String method) {
        this.method = method;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Parameter setId(Long id) {
        this.id = id;
        return this;
    }

    public Bean getModel() {
        return model;
    }

    public Parameter setModel(Bean model) {
        this.model = model;
        return this;
    }

    public Map<?, ?> getMap() {
        return map;
    }

    public Parameter setMap(Map<?, ?> map) {
        this.map = map;
        return this;
    }

    public List<?> getList() {
        return list;
    }

    public Parameter setList(List<?> list) {
        this.list = list;
        return this;
    }
}
