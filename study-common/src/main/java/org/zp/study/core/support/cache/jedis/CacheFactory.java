package org.zp.study.core.support.cache.jedis;

/**
 * @author 赵攀
 *
 */
public class CacheFactory {
    private final static RedisCacheManeger redis=new RedisCacheManeger();
    private final static JedisCacheManager jedis=new JedisCacheManager();
    private CacheFactory(){}
    public static RedisCacheManeger getRedisManeger(){
            return redis;
    }
    public static JedisCacheManager getJedisCacheManager(){
        return jedis;
    }
}
