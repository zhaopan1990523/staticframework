package org.zp.study.core.exception;


import org.zp.study.core.HttpCode;

/**
 * FTP异常
 * 
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:19:19
 * ftp exception
 */
@SuppressWarnings("serial")
public class FtpException extends BaseException {
    public FtpException() {
    }

    public FtpException(String message) {
        super(message);
    }

    public FtpException(String message, Throwable throwable) {
        super(message, throwable);
    }

    protected HttpCode getHttpCode() {
        return HttpCode.INTERNAL_SERVER_ERROR;
    }
}
