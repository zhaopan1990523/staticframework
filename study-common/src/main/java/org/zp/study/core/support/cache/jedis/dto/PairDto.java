package org.zp.study.core.support.cache.jedis.dto;

/**
 * 键值对
 *
 * @param <K> key
 * @param <V> value
 * @author fengjc
 * @version V1.0
 */
public class PairDto<K, V> {

    private K key;
    private V value;

    public PairDto(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}

