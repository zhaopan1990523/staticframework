/**
 * 
 */
package org.zp.study.core.exception;


import org.zp.study.core.HttpCode;

/**
 * 
 * @author zhaopan
 * @version 2017年3月24日 下午9:30:10
 * 实例化错误
 */
@SuppressWarnings("serial")
public class InstanceException extends BaseException {
    public InstanceException() {
        super();
    }

    public InstanceException(Throwable t) {
        super(t);
    }

    protected HttpCode getHttpCode() {
        return HttpCode.INTERNAL_SERVER_ERROR;
    }
}
