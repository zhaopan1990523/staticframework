package org.zp.study.core.exception;


import org.zp.study.core.HttpCode;

/**
 * @author zhaopan
 * @version 2016年5月20日 下午3:19:19
 * 任务相关的Exception
 */
@SuppressWarnings("serial")
public class BusinessException extends BaseException {
	public BusinessException() {
	}

	public BusinessException(Throwable ex) {
		super(ex);
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(String message, Throwable ex) {
		super(message, ex);
	}

	protected HttpCode getHttpCode() {
		return HttpCode.CONFLICT;
	}
}