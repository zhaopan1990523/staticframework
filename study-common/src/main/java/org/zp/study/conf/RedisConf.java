package org.zp.study.conf;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * @author 赵攀

 */
@Configuration
@EnableCaching
public class RedisConf {
}
